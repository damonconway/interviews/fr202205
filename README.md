# Overview
This is a simple ticker API that returns the last n days for a ticker symbol and the average closing price over those days.

# Building
You can build the container using docker-compose.

`docker-compose build`

# Running locally
You can run the container locally using docker-compose.

`docker-compose up`

or if you want to run in the background

`docker-compose up -d`
