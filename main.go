package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"sort"
	"strconv"

	"github.com/gorilla/mux"
)

// Globl variables
var Debug bool
var NDays int
var Port string
var Verbose bool

// Create a struct to hold the data we get back from QueryUrl
type QueryData struct {
	MetaData MetaData              `json:"Meta Data"`
	TSDaily  map[string]TSDailyDay `json:"Time Series (Daily)"`
}

type MetaData struct {
	Info        string `json:"1. Information"`
	Symbol      string `json:"2. Symbol"`
	LastRefresh string `json:"3. Last Refreshed"`
	OutputSize  string `json:"4. Output Size"`
	TimeZone    string `json:"5. Time Zone"`
}

type TSDailyDay struct {
	Open   string `json:"1. open"`
	High   string `json:"2. high"`
	Low    string `json:"3. low"`
	Close  string `json:"4. close"`
	Volume string `json:"5. volume"`
}

// Create a struct to hold our Response
type Response struct {
	AvgClose string                `json:"AvgClose"`
	Days     map[string]TSDailyDay `json:"Days"`
}

// main function
func main() {
	getEnvVars()
	handleRequests()
}

// Helper functions

func print_message(msg string, d bool) {
	if Debug && d {
		fmt.Println(msg)
	} else if (Verbose || Debug) && (d == false) {
		fmt.Println(msg)
	}
}

func message(msg string) {
	print_message(msg, false)
}

func debug_message(msg string) {
	print_message(msg, true)
}

func getEnvVars() {
	debug_message("Function Called: getEnvVars()")

	debugStr, ok := os.LookupEnv("DEBUG")
	if !ok {
		Debug = false
	} else {
		Debug, _ = strconv.ParseBool(debugStr)
	}
	debug_message(fmt.Sprintf("Debug = %t", Debug))

	ndaysStr, ok := os.LookupEnv("NDAYS")
	if !ok {
		ndaysStr = "7"
	}
	NDays, _ = strconv.Atoi(ndaysStr)
	debug_message(fmt.Sprintf("NDays = %d", NDays))

	Port, ok = os.LookupEnv("PORT")
	if !ok {
		Port = "8080"
	}
	Port = fmt.Sprintf(":%s", Port)
	debug_message(fmt.Sprintf("Port = %s", Port))

	verboseStr, ok := os.LookupEnv("VERBOSE")
	if !ok {
		Verbose = false
	} else {
		Verbose, _ = strconv.ParseBool(verboseStr)
	}
	debug_message(fmt.Sprintf("Verbose = %t", Verbose))
}

// End helper functions

// API driver functions

func homePage(w http.ResponseWriter, r *http.Request) {
	message("Endpoint Hit: homePage")

	fmt.Fprintf(w, "Welcome to the Ticker home page!")
}

func handleRequests() {
	myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/", homePage)
	myRouter.HandleFunc("/ticker/dailyAvgClose", getTickerDaysAvgClose).Methods("GET")
	log.Print(http.ListenAndServe(Port, myRouter))
}

// End API driver functions

// Shared API Functions

func getSourceData() *QueryData {
	debug_message("Function Called: getSourceData")

	QueryUrl := os.ExpandEnv("https://www.alphavantage.co/query?apikey=$API_KEY&function=TIME_SERIES_DAILY&symbol=$SYMBOL")

	response, err := http.Get(QueryUrl)
	if err != nil {
		log.Print(err)
	}

	responseData, err := ioutil.ReadAll(response.Body)
	if err != nil {
		log.Print(err)
	}

	var queryDataObj QueryData
	json_err := json.Unmarshal(responseData, &queryDataObj)
	if json_err != nil {
		log.Print(err)
	}

	return &queryDataObj
}

func getNDays(day_list map[string]TSDailyDay) map[string]TSDailyDay {
	debug_message("Function Called: getNDays")

	var days map[string]TSDailyDay
	days = make(map[string]TSDailyDay)

	debug_message("getNDays(): days var created")
	debug_message(fmt.Sprintf("getNDays(): length of day_list = %d", len(day_list)))

	keys := make([]string, 0, len(day_list))
	for k := range day_list {
		keys = append(keys, k)
	}
	sort.Sort(sort.Reverse(sort.StringSlice(keys)))

	debug_message("getNDays(): keys array created")

	for i := 0; i < NDays; i++ {
		debug_message(fmt.Sprintf("getNDays(): day_list loop [%d]", i))

		k := keys[i]
		days[k] = day_list[k]
	}

	debug_message("getNDays(): days var populated")

	return days
}

// End shared functions

// ticker/dailyAvgClose functions
func getTickerDaysAvgClose(w http.ResponseWriter, r *http.Request) {
	message("Endpoint Hit: getTickerDaysAvgClose")

	var resp Response
	responseData := getSourceData()

	daysRequested := getNDays(responseData.TSDaily)
	avgClose := getAvgClose(daysRequested)

	resp.AvgClose = avgClose
	resp.Days = daysRequested

	json_resp := json.NewEncoder(w)
	json_resp.SetIndent("", "  ") // pretty print
	json_resp.Encode(resp)
}

func getAvgClose(days map[string]TSDailyDay) string {
	debug_message("Function Called: getAvgClose")

	var sum float64
	var avg float64

	for k := range days {
		close, err := strconv.ParseFloat(days[k].Close, 64)
		if err != nil {
			log.Print(err)
		}

		sum += close
	}

	avg = sum / float64(len(days))

	return fmt.Sprintf("%f", avg)
}
